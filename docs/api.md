# Service API

## Service API

### Status
Returns current status of service
**Endpoint**
`GET /status`
**Response**
```
status - text with current status
```

## Account API

### Add account
Registers new account to the system
**Endpoint**
`POST /account`
**Json parameters**
```
id - unique account ID
balance - initial account balance
currency - account currency
```
**Response**
Account entity:
```
id - account ID
balance - account balance
currency - account currency
```

### Get all accounts
Returns all registered account
**Endpoint**
`GET /account`
**Response**
List of accounts (see above)

## Wallet API

### Get all account payments
Returns all payments for given account
**Endpoint**
`GET /payments/{accountID}`
**Query parameters**
```
accountID - account ID
```
**Response**
List of account payment (see description below)
Account payment entity:
```
from_account - account ID for incoming payment
to_account - account ID for outgoing payment
amount - payment amount
direction - "outgoing" or "incoming"
```

### Issue payment
Issues new payment between two accounts with the same currency
**Endpoint**
`POST /pay`
**Json parameters**
```
id - unique account ID
balance - initial account balance
currency - account currency
```
**Response**
Account entity:
```
from_account - account ID to decrease amount
to_account - account ID to increase amount
amount - payment amount
```
