package common

import (
	"errors"
	"fmt"
)

type ErrorType struct {
	error
}

func NewErrorType(name string) *ErrorType {
	return &ErrorType{
		error: errors.New(name),
	}
}

func (e *ErrorType) New(msg string) error {
	return fmt.Errorf("%w: %v", e, msg)
}

func (e *ErrorType) Newf(format string, a ...interface{}) error {
	msg := fmt.Sprintf(format, a)
	return fmt.Errorf("%w: %v", e, msg)
}

func (e *ErrorType) NewWithNoMessage() error {
	return fmt.Errorf("%w", e)
}

var ErrNotFound = NewErrorType("not_found")
var ErrInvalidArgument = NewErrorType("invalid_argument")
var ErrIllegalState = NewErrorType("illegal_state")
var ErrGeneralError = NewErrorType("general_error")
