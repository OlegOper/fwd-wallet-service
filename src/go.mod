module coin.io/wallet

go 1.13

require (
	github.com/go-kit/kit v0.9.0
	github.com/go-logfmt/logfmt v0.5.0 // indirect
	github.com/gorilla/mux v1.7.3
	github.com/jinzhu/gorm v1.9.12
	github.com/lib/pq v1.3.0
	github.com/shopspring/decimal v0.0.0-20200105231215-408a2507e114
	github.com/stretchr/testify v1.4.0
	go.uber.org/dig v1.8.0
)
