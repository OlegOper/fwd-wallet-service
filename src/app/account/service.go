package account

import (
	"github.com/jinzhu/gorm"
)

// Service for accounts management
type Service interface {
	CreateAccount(acc *Account) error
	GetAllAccounts() ([]*Account, error)
	GetAccountByID(id AccountID) (*Account, error)
}

type service struct {
	db  *gorm.DB
	dao DAO
}

var _ Service = (*service)(nil)

func NewService(
	db *gorm.DB,
	dao DAO,
) Service {

	return &service{
		db:  db,
		dao: dao,
	}
}

// Creates new user account
func (s *service) CreateAccount(acc *Account) error {
	return s.dao.InsertAccount(s.db, acc)
}

// Returns all available accounts
func (s *service) GetAllAccounts() ([]*Account, error) {
	return s.dao.FindAllAccounts(s.db)
}

// Returns account by ID
func (s *service) GetAccountByID(id AccountID) (*Account, error) {
	return s.dao.FindAccountByID(s.db, id, false)
}
