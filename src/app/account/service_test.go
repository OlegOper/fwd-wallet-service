package account_test

import (
	"errors"
	"testing"

	"coin.io/wallet/app/account"
	"coin.io/wallet/app/testutils"
	"coin.io/wallet/common"
	"github.com/shopspring/decimal"
	"github.com/stretchr/testify/suite"
)

type ServiceTestSuite struct {
	testutils.DBTestSuite
	Service      account.Service
	TestAccounts []*account.Account
}

func (s *ServiceTestSuite) SetupSuite() {
	s.DBTestSuite.SetupSuite()

	s.Container.Invoke(func(service account.Service) {
		s.Service = service
	})

	s.TestAccounts = []*account.Account{
		{
			ID:       "bob123",
			Balance:  decimal.New(100, 0),
			Currency: "USD",
		},
		{
			ID:       "alice456",
			Balance:  decimal.New(1, -2),
			Currency: "USD",
		},
	}
}

func TestServiceTestSuite(t *testing.T) {
	suite.Run(t, &ServiceTestSuite{})
}

func (s *ServiceTestSuite) TestCreateGetAllAcounts() {
	accounts, err := s.Service.GetAllAccounts()
	s.Require().NoError(err)
	s.Require().ElementsMatch([]*account.Account{}, accounts)

	acc1 := s.TestAccounts[0]
	err = s.Service.CreateAccount(acc1)
	s.Require().NoError(err)

	accounts, err = s.Service.GetAllAccounts()
	s.Require().NoError(err)
	s.Require().ElementsMatch([]*account.Account{acc1}, accounts)

	acc2 := s.TestAccounts[1]
	err = s.Service.CreateAccount(acc2)
	s.Require().NoError(err)

	accounts, err = s.Service.GetAllAccounts()
	s.Require().NoError(err)
	s.Require().ElementsMatch([]*account.Account{acc1, acc2}, accounts)
}

func (s *ServiceTestSuite) TestCreateAcountFailsOnDuplicate() {
	acc := s.TestAccounts[0]
	err := s.Service.CreateAccount(acc)
	s.Require().NoError(err)

	err = s.Service.CreateAccount(acc)
	s.Require().Error(err)
	s.Require().True(errors.Is(err, common.ErrInvalidArgument))
}

func (s *ServiceTestSuite) TestGetAccountByID() {
	acc1 := s.TestAccounts[0]
	acc2 := s.TestAccounts[1]

	acc, err := s.Service.GetAccountByID(acc1.ID)
	s.Require().Error(err)
	s.Require().True(errors.Is(err, common.ErrNotFound))
	s.Require().Nil(acc)

	err = s.Service.CreateAccount(acc1)
	s.Require().NoError(err)

	acc, err = s.Service.GetAccountByID(acc1.ID)
	s.Require().NoError(err)
	s.Require().Equal(acc1, acc)

	acc, err = s.Service.GetAccountByID(acc2.ID)
	s.Require().Error(err)
	s.Require().True(errors.Is(err, common.ErrNotFound))
	s.Require().Nil(acc)

	err = s.Service.CreateAccount(acc2)
	s.Require().NoError(err)

	acc, err = s.Service.GetAccountByID(acc1.ID)
	s.Require().NoError(err)
	s.Require().Equal(acc1, acc)
}
