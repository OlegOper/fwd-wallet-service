package account

import "github.com/shopspring/decimal"

type Currency string

type Money = decimal.Decimal

type AccountID string

func AccountIDFromString(id string) AccountID {
	return AccountID(id)
}

// User account. Used for single currency usage
type Account struct {
	ID       AccountID `json:"id" gorm:"primary_key"`
	Balance  Money     `json:"balance" sql:"type:decimal(20,4);"`
	Currency Currency  `json:"currency"`
}
