package status

import (
	"context"
)

type Service interface {
	Status(ctx context.Context) (string, error)
}

type service struct{}

var _ Service = (*service)(nil)

func NewService() Service {
	return &service{}
}

// Returns current status of of application
func (s *service) Status(ctx context.Context) (string, error) {
	return "ok", nil
}
