package wallet

import (
	"coin.io/wallet/app/account"
)

type PaymentID int64

type Direction string

const (
	DirectionNone     Direction = ""
	DirectionIncoming Direction = "incoming"
	DirectionOutgoing Direction = "outgoing"
)

// Payment between two accounts
type Payment struct {
	ID          PaymentID         `json:"id"`
	FromAccount account.AccountID `json:"from_account"`
	ToAccount   account.AccountID `json:"to_account"`
	Amount      account.Money     `json:"amount" sql:"type:decimal(20,4);"`
}

// Payment view for particular account
type AccountPayment struct {
	FromAccount account.AccountID `json:"from_account,omitempty"`
	ToAccount   account.AccountID `json:"to_account,omitempty"`
	Amount      account.Money     `json:"amount"`
	Direction   Direction         `json:"direction"`
}

// Converts Payment to a payment view
func (p *Payment) ToAccountPayment(direction Direction) *AccountPayment {
	res := &AccountPayment{
		Amount:    p.Amount,
		Direction: direction,
	}

	if direction == DirectionIncoming {
		res.FromAccount = p.FromAccount
	} else {
		res.ToAccount = p.ToAccount
	}

	return res
}
