package wallet_test

import (
	"errors"
	"testing"

	"coin.io/wallet/app/account"
	"coin.io/wallet/app/testutils"
	"coin.io/wallet/app/wallet"
	"coin.io/wallet/common"
	"github.com/shopspring/decimal"
	"github.com/stretchr/testify/suite"
)

type ServiceTestSuite struct {
	testutils.DBTestSuite
	AccountService account.Service
	WalletService  wallet.Service
	TestAccounts   []*account.Account
}

func (s *ServiceTestSuite) SetupSuite() {
	s.DBTestSuite.SetupSuite()

	s.Container.Invoke(func(accountService account.Service, walletService wallet.Service) {
		s.AccountService = accountService
		s.WalletService = walletService
	})

	s.TestAccounts = []*account.Account{
		{
			ID:       "bob123",
			Balance:  decimal.New(100, 0),
			Currency: "USD",
		},
		{
			ID:       "alice456",
			Balance:  decimal.New(1, -2),
			Currency: "USD",
		},
		{
			ID:       "eva789",
			Balance:  decimal.New(1000, 0),
			Currency: "EUR",
		},
	}
}

func TestServiceTestSuite(t *testing.T) {
	suite.Run(t, &ServiceTestSuite{})
}

func (s *ServiceTestSuite) TestPay() {
	acc1 := s.TestAccounts[0]
	acc2 := s.TestAccounts[1]

	initialBalance1 := acc1.Balance
	initialBalance2 := acc2.Balance

	err := s.AccountService.CreateAccount(acc1)
	s.Require().NoError(err)
	err = s.AccountService.CreateAccount(acc2)
	s.Require().NoError(err)

	amount := decimal.New(50, 0)
	payment, err := s.WalletService.Pay(acc1.ID, acc2.ID, amount)
	s.Require().NoError(err)
	s.Require().NotEqual(0, payment.ID)

	payment.ID = 0
	s.Require().Equal(payment, &wallet.Payment{
		FromAccount: acc1.ID,
		ToAccount:   acc2.ID,
		Amount:      amount,
	})

	acc1, err = s.AccountService.GetAccountByID(acc1.ID)
	s.Require().NoError(err)
	s.Require().Equal(acc1.Balance, initialBalance1.Sub(amount))

	acc2, err = s.AccountService.GetAccountByID(acc2.ID)
	s.Require().NoError(err)
	s.Require().Equal(acc2.Balance, initialBalance2.Add(amount))

	// Insufficient funds
	amount = decimal.New(150, 0)
	payment, err = s.WalletService.Pay(acc1.ID, acc2.ID, amount)
	s.Require().Nil(payment)
	s.Require().Error(err)
	s.Require().True(errors.Is(err, common.ErrInvalidArgument))

	// Invalid currency
	acc3 := s.TestAccounts[2]
	err = s.AccountService.CreateAccount(acc3)
	s.Require().NoError(err)
	amount = decimal.New(10, 0)
	payment, err = s.WalletService.Pay(acc1.ID, acc3.ID, amount)
	s.Require().Nil(payment)
	s.Require().Error(err)
	s.Require().True(errors.Is(err, common.ErrInvalidArgument))
}

func (s *ServiceTestSuite) TestGetAllAccountPayments() {
	acc1 := s.TestAccounts[0]
	acc2 := s.TestAccounts[1]

	payments, err := s.WalletService.GetAllAccountPayments(acc1.ID)
	s.Require().NoError(err)
	s.Require().ElementsMatch([]*wallet.AccountPayment{}, payments)

	err = s.AccountService.CreateAccount(acc1)
	s.Require().NoError(err)
	err = s.AccountService.CreateAccount(acc2)
	s.Require().NoError(err)

	amount1 := decimal.New(50, 0)
	_, err = s.WalletService.Pay(acc1.ID, acc2.ID, amount1)
	s.Require().NoError(err)

	amount2 := decimal.New(20, 0)
	_, err = s.WalletService.Pay(acc2.ID, acc1.ID, amount2)
	s.Require().NoError(err)

	payments, err = s.WalletService.GetAllAccountPayments(acc1.ID)
	s.Require().NoError(err)
	s.Require().ElementsMatch([]*wallet.AccountPayment{
		{
			ToAccount: acc2.ID,
			Amount:    amount1,
			Direction: wallet.DirectionOutgoing,
		},
		{
			FromAccount: acc2.ID,
			Amount:      amount2,
			Direction:   wallet.DirectionIncoming,
		},
	}, payments)

	payments, err = s.WalletService.GetAllAccountPayments(acc2.ID)
	s.Require().NoError(err)
	s.Require().ElementsMatch([]*wallet.AccountPayment{
		{
			FromAccount: acc1.ID,
			Amount:      amount1,
			Direction:   wallet.DirectionIncoming,
		},
		{
			ToAccount: acc1.ID,
			Amount:    amount2,
			Direction: wallet.DirectionOutgoing,
		},
	}, payments)
}
