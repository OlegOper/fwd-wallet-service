package wallet

import (
	"coin.io/wallet/app/account"
	"coin.io/wallet/common"
	"github.com/jinzhu/gorm"
)

// Data layer for payments management
type DAO interface {
	InsertPayment(tx *gorm.DB, payment *Payment) error
	FindAllPaymentsByAccount(tx *gorm.DB, accountID account.AccountID) ([]*Payment, error)
}

type dao struct {
}

var _ DAO = (*dao)(nil)

func NewDAO() DAO {
	return &dao{}
}

// Inserts new payment in DB
func (dao *dao) InsertPayment(tx *gorm.DB, payment *Payment) error {
	if err := tx.Create(payment).Error; err != nil {
		return common.ErrGeneralError.New(err.Error())
	}
	return nil
}

// Finds all payments where account ID on "from" or "to" side
func (dao *dao) FindAllPaymentsByAccount(tx *gorm.DB, accountID account.AccountID) ([]*Payment, error) {
	var res []*Payment
	err := tx.Where("from_account = ?", accountID).Or("to_account = ?", accountID).Find(&res).Error
	if err != nil {
		return nil, err
	}
	return res, nil
}
