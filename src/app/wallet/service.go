package wallet

import (
	"coin.io/wallet/app/account"
	"coin.io/wallet/common"
	"github.com/jinzhu/gorm"
)

// Service for payments management
type Service interface {
	Pay(from, to account.AccountID, amount account.Money) (*Payment, error)
	GetAllAccountPayments(id account.AccountID) ([]*AccountPayment, error)
}

type service struct {
	db             *gorm.DB
	accountDAO     account.DAO
	accountService account.Service
	walletDAO      DAO
}

var _ Service = (*service)(nil)

func NewService(
	db *gorm.DB,
	accountDAO account.DAO,
	accountService account.Service,
	walletDAO DAO,
) Service {

	return &service{
		db:             db,
		accountDAO:     accountDAO,
		accountService: accountService,
		walletDAO:      walletDAO,
	}
}

// Performs payment between two accounts with the same currency
func (s *service) Pay(from, to account.AccountID, amount account.Money) (*Payment, error) {
	var payment *Payment

	err := s.db.Transaction(func(tx *gorm.DB) error {
		fromAcc, err := s.accountDAO.FindAccountByID(tx, from, true)
		if err != nil {
			return err
		}
		if fromAcc.Balance.LessThanOrEqual(amount) {
			return common.ErrInvalidArgument.New("Insufficient funds")
		}

		toAcc, err := s.accountDAO.FindAccountByID(tx, to, true)
		if err != nil {
			return err
		}

		if fromAcc.Currency != toAcc.Currency {
			return common.ErrInvalidArgument.New("Accounts currency doesn't match")
		}

		fromAcc, err = s.accountDAO.UpdateBalance(tx, fromAcc, fromAcc.Balance.Sub(amount))
		if err != nil {
			return err
		}

		toAcc, err = s.accountDAO.UpdateBalance(tx, toAcc, toAcc.Balance.Add(amount))
		if err != nil {
			return err
		}

		payment = &Payment{
			FromAccount: from,
			ToAccount:   to,
			Amount:      amount,
		}
		return s.walletDAO.InsertPayment(tx, payment)
	})

	if err != nil {
		return nil, err
	}

	return payment, nil
}

// Returns all payments for given account
func (s *service) GetAllAccountPayments(id account.AccountID) ([]*AccountPayment, error) {
	payments, err := s.walletDAO.FindAllPaymentsByAccount(s.db, id)
	if err != nil {
		return nil, err
	}

	var res []*AccountPayment

	for _, it := range payments {
		if it.FromAccount == id {
			res = append(res, it.ToAccountPayment(DirectionOutgoing))
		} else if it.ToAccount == id {
			res = append(res, it.ToAccountPayment(DirectionIncoming))
		}
	}

	return res, nil
}
