package service

import (
	"go.uber.org/dig"
)

func Provide(container *dig.Container) {
	container.Provide(NewEndpoints)
	container.Provide(NewHTTPServer)
}
