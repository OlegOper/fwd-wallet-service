package service

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"

	"coin.io/wallet/app/account"
	"coin.io/wallet/app/wallet"
	"coin.io/wallet/common"
	"github.com/gorilla/mux"
)

type statusRequest struct{}

type statusResponse struct {
	Status string `json:"status"`
}

type addAccountRequest struct {
	*account.Account
}

type addAccountResponse struct {
	*account.Account
}

type getAllAccountsRequest struct {
}

type getAllAccountPaymentsRequest struct {
	AccountID account.AccountID
}

type getAllAccountPaymentsResponse struct {
	AccountPayments []*wallet.AccountPayment `json:"account_payments"`
}

type payRequest struct {
	FromAccount account.AccountID `json:"from_account"`
	ToAccount   account.AccountID `json:"to_account"`
	Amount      account.Money     `json:"amount"`
}

type payResponse struct {
	*wallet.Payment
}

type getAllAccountsResponse struct {
	Accounts []*account.Account `json:"accounts"`
}

func decodeStatusRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req statusRequest
	return req, nil
}

func decodeAddAccountRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req addAccountRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		return nil, err
	}
	return req, nil
}

func decodeGetAllAcountsRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req getAllAccountsRequest
	return req, nil
}

func decodeGetAllAccountPaymentsRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	vars := mux.Vars(r)
	return getAllAccountPaymentsRequest{
		AccountID: account.AccountIDFromString(vars["accountID"]),
	}, nil
}

func decodePayRequest(ctx context.Context, r *http.Request) (interface{}, error) {
	var req payRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		return nil, err
	}
	return req, nil
}

func encodeResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	if e, ok := response.(errorer); ok && e.error() != nil {
		// Not a Go kit transport error, but a business-logic error.
		// Provide those as HTTP errors.
		encodeErrorResponse(ctx, e.error(), w)
		return nil
	}
	return json.NewEncoder(w).Encode(response)
}

type errorer interface {
	error() error
}

func encodeErrorResponse(ctx context.Context, err error, w http.ResponseWriter) {
	if err == nil {
		panic("encodeError with nil error")
	}
	w.WriteHeader(codeFrom(err))
	json.NewEncoder(w).Encode(map[string]interface{}{
		"error": err.Error(),
	})
}

func codeFrom(err error) int {
	if errors.Is(err, common.ErrNotFound) {
		return 404
	}
	if errors.Is(err, common.ErrInvalidArgument) {
		return 400
	}
	if errors.Is(err, common.ErrIllegalState) || errors.Is(err, common.ErrGeneralError) {
		return 500
	}
	return 500
}
