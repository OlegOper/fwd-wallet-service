package service

import (
	"context"

	"coin.io/wallet/app/account"
	"coin.io/wallet/app/status"
	"coin.io/wallet/app/wallet"
	"coin.io/wallet/common"
	"github.com/go-kit/kit/endpoint"
)

func NewEndpoints(
	accountService account.Service,
	statusService status.Service,
	walletService wallet.Service,
) *Endpoints {

	return &Endpoints{
		StatusEndpoint:                NewStatusEndpoint(statusService),
		AddAccountEndpoint:            NewAddAccountEndpoint(accountService),
		GetAllAccountsEndpoint:        NewGetAllAccountsEndpoint(accountService),
		GetAllAccountPaymentsEndpoint: NewGetAllAccountPaymentsEndpoint(walletService),
		PayEndpoint:                   NewPayEndpoint(walletService),
	}
}

// Service endpoints
type Endpoints struct {
	StatusEndpoint                endpoint.Endpoint
	AddAccountEndpoint            endpoint.Endpoint
	GetAllAccountsEndpoint        endpoint.Endpoint
	GetAllAccountPaymentsEndpoint endpoint.Endpoint
	PayEndpoint                   endpoint.Endpoint
}

func NewStatusEndpoint(statusService status.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		_ = request.(statusRequest)
		s, err := statusService.Status(ctx)
		if err != nil {
			return statusResponse{s}, err
		}
		return statusResponse{s}, nil
	}
}

func NewAddAccountEndpoint(accountService account.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(addAccountRequest)
		if req.Account == nil {
			return nil, common.ErrInvalidArgument.New("Account is not defined")
		}
		err := accountService.CreateAccount(req.Account)
		if err != nil {
			return nil, err
		}
		return addAccountResponse{
			Account: req.Account,
		}, nil
	}
}

func NewGetAllAccountsEndpoint(accountService account.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		_ = request.(getAllAccountsRequest)
		accs, err := accountService.GetAllAccounts()
		if err != nil {
			return nil, err
		}
		return getAllAccountsResponse{
			Accounts: accs,
		}, nil
	}
}

func NewGetAllAccountPaymentsEndpoint(walletService wallet.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(getAllAccountPaymentsRequest)
		payments, err := walletService.GetAllAccountPayments(req.AccountID)
		if err != nil {
			return nil, err
		}
		return getAllAccountPaymentsResponse{
			AccountPayments: payments,
		}, nil
	}
}

func NewPayEndpoint(walletService wallet.Service) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(payRequest)
		payment, err := walletService.Pay(req.FromAccount, req.ToAccount, req.Amount)
		if err != nil {
			return nil, err
		}
		return payResponse{
			Payment: payment,
		}, nil
	}
}
