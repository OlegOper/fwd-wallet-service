package service

import (
	"net/http"

	httptransport "github.com/go-kit/kit/transport/http"
	"github.com/gorilla/mux"
)

func NewHTTPServer(endpoints *Endpoints) http.Handler {
	r := mux.NewRouter()
	r.Use(commonMiddleware)

	errorEncoder := httptransport.ServerErrorEncoder(encodeErrorResponse)
	options := []httptransport.ServerOption{errorEncoder}

	r.Methods("GET").Path("/status").Handler(httptransport.NewServer(
		endpoints.StatusEndpoint,
		decodeStatusRequest,
		encodeResponse,
		options...,
	))

	r.Methods("POST").Path("/account").Handler(httptransport.NewServer(
		endpoints.AddAccountEndpoint,
		decodeAddAccountRequest,
		encodeResponse,
		options...,
	))

	r.Methods("GET").Path("/account").Handler(httptransport.NewServer(
		endpoints.GetAllAccountsEndpoint,
		decodeGetAllAcountsRequest,
		encodeResponse,
		options...,
	))

	r.Methods("GET").Path("/payments/{accountID}").Handler(httptransport.NewServer(
		endpoints.GetAllAccountPaymentsEndpoint,
		decodeGetAllAccountPaymentsRequest,
		encodeResponse,
		options...,
	))

	r.Methods("POST").Path("/pay").Handler(httptransport.NewServer(
		endpoints.PayEndpoint,
		decodePayRequest,
		encodeResponse,
		options...,
	))

	return r
}

func commonMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Add("Content-Type", "application/json")
		next.ServeHTTP(w, r)
	})
}
